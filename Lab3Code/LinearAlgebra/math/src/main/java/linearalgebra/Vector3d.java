// Thomas Proctor | 2136365 | :D

package linearalgebra;

public class Vector3d {
    
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double Magnitude() {
        return Math.sqrt((this.x * this.x)+(this.y * this.y)+(this.z * this.z));
    }

    public double dotProduct(Vector3d secondVector) {
        return (this.x*secondVector.x) + (this.y*secondVector.y) + (this.z*secondVector.z);
    }

    public Vector3d add(Vector3d secondVector) {

        Vector3d newVector = new Vector3d(this.x+secondVector.x, this.y+secondVector.y, this.z+secondVector.z);
        return newVector;
    }
}
