// Thomas Proctor | 2136365 | :D
// This was just for doing a quick test
package linearalgebra;

public class main {
    public static void main(String[] args) {
        Vector3d a = new Vector3d(1, 1, 2);
        Vector3d b = new Vector3d(2, 3, 4);

        System.out.println(a.dotProduct(b));
    }
}
