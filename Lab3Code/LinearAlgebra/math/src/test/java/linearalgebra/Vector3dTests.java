// Thomas Proctor | 2136365 | :D

package linearalgebra;

import org.junit.Test;
import static org.junit.Assert.*;

public class Vector3dTests {
        @Test
        public void testGetMethods() {
            Vector3d testVector = new Vector3d(1, 2, 3);
            assertEquals(testVector.getX(), 1, .001);
            assertEquals(testVector.getY(), 2, .001);
            assertEquals(testVector.getZ(), 3, .001);
        }
    
        @Test
        public void testMagnitude() {
            Vector3d testVector = new Vector3d(1, 2, 3);
            assertEquals(testVector.Magnitude(), 3.74165739, .0001);
        }

        @Test
        public void testDotProduct() {
            Vector3d testVector = new Vector3d(1, 1, 2);
            Vector3d testVector2 = new Vector3d(2, 3, 4);
            assertEquals(testVector.dotProduct(testVector2), 13, .001);
        }

        @Test
        public void testVectorAddition() {
            Vector3d testVector = new Vector3d(1, 1, 2);
            Vector3d testVector2 = new Vector3d(2, 3, 4);
            Vector3d testVector3 = testVector.add(testVector2);
            
            assertEquals(testVector3.getX(), 3, .001);
            assertEquals(testVector3.getY(), 4, .001);
            assertEquals(testVector3.getZ(), 6, .001);
        }

}
